#
# PiCam-NN
## with neural network person detection

Thanks and credit goes to [PiSimo](https://github.com/PiSimo/PiCamNN "github.com/PiSimo/PiCamNN")
who created this project. 
My version is just an evolution of his great project to satisfy my needs.

PiCam-NN is a self-made surveillance system, designed to run on a Raspberry Pi 3B.

See (https://gitlab.com/kralle71/picam) for more informations and the full repo.


What you need:
* Server (Pi will do)
* armv8 / arm64 / aarch64 OS (eg. Fedora/Ubuntu)
* docker and docker compose (or docker swarm)
* Outdoor IP Cam with RTSP Stream (USB-Webcam will do)
* Telegram Bot (or Email)

_My Setup (Raspberry Pi 3B, Outdoor IP Cam with RTSP Stream)
runs with 1 FPS to reduce CPU load of the Pi. 4 FPS with Pi3B is possible,
but will result in 100% load 24/7. For my task 1 FPS is sufficient.
If you need more FPS, you need a more powerful server._

## installation instructions

```bash
git clone https://gitlab.com/kralle71/picam_docker.git
```
```bash
cd picam_docker
```

change permissions for nginx docker volume mount
```bash
sudo chmod 755 -R ./www
```
configure PiCam with preferred editor
```bash
mv config_template.yaml config.yaml
```
```bash
nano config.yaml
```
start
```bash
docker-compose up
```
watch videos at
```bash
http://localhost:80
```
