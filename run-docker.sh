#!/bin/bash
docker run -d --rm --name picam \
    -v ${PWD}/config.yaml:/app/config.yaml \
    -v ${PWD}/picam.log:/app/picam.log \
    -v ${PWD}/telegram_bot:/app/telegram_bot \
    -v ${PWD}/www:/www \
	 kralle71/picam:latest